#!/bin/bash

# $1: The var to replace
# $2: The new value
# $3: The file to run on
# e.g. set_template_var PATH /home/caleb/blah /redditNotify.service
set_template_var() {
	set -x
	sed "s*%$1%*$2*g" -i $3
	set +x
}

die() {
	echo "FATAL: $1"
	[[ -z $2 ]] && exit 1
	exit $2
}

# Ask a question until a suitable answer is given
# $1: the question
# $2: the answer
ask() {
	local __resultvar=$2
	local result=""
	echo "$1"
	while [ -z "$result" ]; do
		read -p "> " result
	done
	eval $__resultvar="'$result'"
}

OUTDIR=""

echo "Please pick an install directory, e.g. \"/home/caleb/projects/redditNotify\""
echo "Please enter a FULL path"
echo "Aliases like \"~\" won't work"
echo "NOTE: To move the bot after deploying you must edit the service file"
while [ -z $OUTDIR ] || [ -d $OUTDIR ]; do
	read -p "> " OUTDIR
	if [ -d $OUTDIR ]; then
		echo "'$OUTDIR' already exists, please delete it or choose a different output folder."
	fi
done

echo "Will configure bot in '$OUTDIR'"
mkdir -p $OUTDIR

if [ ! -e .setupdone ]; then
	echo "This is the first time you've run this script, installing dependencies with pip"
	set -x
	pip install --user -r requirements.txt
	set +x
	touch .setupdone
fi

cp config.toml $OUTDIR/config.toml
cp config.secret.template.toml $OUTDIR/config.secret.toml
cp redditNotify.template.service $OUTDIR/redditNotify.service
cp redditNotify.py $OUTDIR/redditNotify.py

pushd $OUTDIR > /dev/null

echo 
echo 
echo "Please go to https://www.reddit.com/prefs/apps/, click on 'create an app' at the bottom and fill out the form."
echo "Use \"http://localhost:8080\" for both the URLs, the URLs will not be used for the bot."
echo "Press enter to continue"
read

ask "Enter the client ID from reddit" CLIENTID
ask "Enter the client secret from reddit" CLIENTSECRET
ask "Enter your Reddit username" CLIENTUSERNAME

echo "Please go to https://ifttt.com/maker_webhooks, then create a webhook. Install the IFTTT app on your phone and configure the webhook as follows:"
ask "Enter the string of random characters at the end of the webhook URL you're given" WEBHOOKKEY
echo "
Event Name: freelance_post
Message: Value2 (that is, from the list, not the string 'Value2')
Title: Value1
Link URL: Value3
"
echo "Press enter to continue"
read

set_template_var "CLIENTID" "$CLIENTID" config.secret.toml
set_template_var "CLIENTSECRET" "$CLIENTSECRET" config.secret.toml
set_template_var "CLIENTUSERNAME" "$CLIENTUSERNAME" config.secret.toml
set_template_var "WEBHOOKKEY" "$WEBHOOKKEY" config.secret.toml

echo "Configuring service file for current user"
set_template_var "USER" "$USER" redditNotify.service
set_template_var "PATH" "$OUTDIR" redditNotify.service

echo
echo "Pushing systemd service to \"~/.local/share/systemd/user/redditNotify.service\""
set -x
mkdir -p ~/.local/share/systemd/user/
cp redditNotify.service ~/.local/share/systemd/user/redditNotify.service
set +x
echo "Enabling and starting service"

popd > /dev/null

set -x
systemctl --user enable redditNotify.service
systemctl --user start redditNotify.service
set +x

echo 
echo -e "\e[32m===================\e[0m"
echo -e "\e[32m    ALL DONE!\e[0m"
echo -e "\e[32m===================\e[0m"

echo "Please edit \"$OUTDIR/config.toml\" to configure the bot"