#!/bin/env python3
# Copyright (c) 2020 Caleb Connolly <caleb@connolly.tech>
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import toml
import praw
from ifttt_webhook import IftttWebhook

config = toml.load('config.toml')
config_secret = toml.load('config.secret.toml')
r_config = config["reddit"]
r_secret_config = config_secret["reddit"]
n_config = config_secret["notify"]

reddit = None

def reddit_init():
    global reddit
    reddit = praw.Reddit(client_id=r_secret_config["client_id"],
        client_secret=r_secret_config["client_secret"],
        user_agent="Notifier by /u/"+r_secret_config["user_username"])

def reddit_subscribe():
    subreddits = [s["name"] for s in r_config["subreddits"]]
    print("Streaming: "+", ".join(subreddits))
    for submission in reddit.subreddit("+".join(subreddits)).stream.submissions():
        yield submission

def reddit_config_for_subreddit(subreddit):
    return r_config["subreddits"][[s["name"] for s in r_config["subreddits"]].index(subreddit)]


# Return true if this submission matches the whitelist
def reddit_submission_search(submission):
    words = r_config["search_whitelist"]
    if len(words) == 0:
        return True
    # Get the config for the subreddit for this submission
    sub_config = reddit_config_for_subreddit(submission.subreddit)
    if sub_config["whitelist_disable"]:
        return True
    if not r_config["show_forhire"] and "[for hire]" in submission.title.lower():
        return False
    for word in words:
        if word in submission.title.lower():
            return True
    search_description = r_config["search_title_only"]
    if search_description:
        for word in words:
            if word in submission.selftext.lower():
                return True
    return False

def notify(submission):
    sub_config = reddit_config_for_subreddit(submission.subreddit)
    key = n_config["webhook_key"]

    print(submission.subreddit, ":", submission.permalink)
    
    success = False
    while not success:
        try:
            ifttt = IftttWebhook(key)
            ifttt.trigger('freelance_post', value1=submission.title, value2=submission.selftext, value3="https://reddit.com"+submission.permalink)
            success = True
        except Exception:
            print("ERROR: Failed to send webhook: "+submission.title)
            print("Trying again in 5 seconds")
            time.sleep(5)

def main():
    reddit_init()
    for submission in reddit_subscribe():
        if (reddit_submission_search(submission)):
            notify(submission)
        #print(submission.subreddit, ":", submission.title)





if __name__ == "__main__":
    main()
