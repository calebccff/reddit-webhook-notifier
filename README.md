# redditNotify

A simple python script to stream new reddit posts from your choice of subreddits, check for matching terms, and send you a notification.

## Requirements

* A computer running Python with pip installed, the build scripts assumes you're running systemd
* An IFTTT account
* A reddit account

## Instructions

```bash
git clone https://gitlab.com/calebccff/reddit-webhook-notifier
cd reddit-webhook-notifier
./build.sh
```

Then edit the `config.toml` file in the install directory.

```
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```